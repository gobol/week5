       IDENTIFICATION DIVISION. 
       PROGRAM-ID. LIST-6-4.
       AUTHOR.  THANTHAT.
       

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 COUNTERS.
          05 HUNDRED-COUNT  PIC 99 VALUE ZERO.
          05 TEN-COUNT      PIC 99 VALUE ZERO.
          05 UNITS-COUNT    PIC 99 VALUE ZERO.
       01 ODOMETER.
          05 PRN-HUNDREDS   PIC 9.
          05 FILLER         PIC X  VALUE "-".
          05 PRN-TENS       PIC 9.
          05 FILLER         PIC X  VALUE "-".
          05 PRN-UNITS      PIC 9.
          05 FILLER         PIC X  VALUE "-".
       
       PROCEDURE DIVISION .
       000-BEGIN.
           DISPLAY "Using an out-of-line Perform"
           PERFORM 001-COUNT-MILEAGE THRU 001-EXIT
               VARYING HUNDRED-COUNT FROM 0 BY 1
               UNTIL HUNDRED-COUNT > 9
               AFTER TEN-COUNT FROM 0 BY 1
               UNTIL TEN-COUNT > 9
               AFTER UNITS-COUNT FROM 0 BY 1
               UNTIL UNITS-COUNT > 9

      *    แบบที่ 2
      *    PERFORM VARYING HUNDRED-COUNT FROM 0 BY 1
      *       UNTIL HUNDRED-COUNT > 9
      *            PERFORM VARYING TEN-COUNT FROM 0 BY 1
      *               UNTIL TEN-COUNT > 9
      *                    PERFORM VARYING UNITS-COUNT FROM 0 BY 1
      *                       UNTIL UNITS-COUNT > 9
      *                            MOVE HUNDRED-COUNT TO PRN-HUNDREDS
      *                            MOVE TEN-COUNT TO PRN-TENS 
      *                            MOVE UNITS-COUNT TO PRN-UNITS  
      *                            DISPLAY "Out -" ODOMETER 
      *                    END-PERFORM
      *            END-PERFORM
      *    END-PERFORM
           GOBACK
           .


       001-COUNT-MILEAGE.
           MOVE HUNDRED-COUNT TO PRN-HUNDREDS
           MOVE TEN-COUNT TO PRN-TENS 
           MOVE UNITS-COUNT TO PRN-UNITS  
           DISPLAY "Out -" ODOMETER 

           . 

       001-EXIT.
           EXIT
           .