       IDENTIFICATION DIVISION. 
       PROGRAM-ID. square-star.
       AUTHOR. THANTHAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  SCR-LINE PIC X(80) VALUE SPACES .
       01  STAR-NUM PIC 9(3) VALUE 0.


       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 002-INPUT-STAR-NUM  THRU 002-EXIT 
           PERFORM  001-PRINT-STAR-INLINE THRU 001-EXIT 10 TIMES 
           
           GOBACK 
       .
       
       001-PRINT-STAR-INLINE.
           MOVE ALL "*" TO SCR-LINE(1:10)
           DISPLAY SCR-LINE 
       .

       001-EXIT.
           EXIT
       .

       002-INPUT-STAR-NUM.
           PERFORM  UNTIL  STAR-NUM  > 0
              DISPLAY  "PLEASE INPUT STAR NUMBER: " WITH NO ADVANCING 
              ACCEPT STAR-NUM
              IF STAR-NUM =0 DISPLAY "PLEASE INPUT STAR NUMBER IN POSITI
      -        "VE NUMBER "
           END-PERFORM
           
       .
       002-EXIT.
           EXIT
       .