       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRIANGLE-1.
       AUTHOR. THANTHAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 SCR-LINE           PIC X(80) VALUE SPACES.
       01 STAR-NUM           PIC 9(3)  VALUE ZEROS.
          88 VALID-STAR-NUM            VALUE 1 THRU 80.
       01 INDEX-NUM          PIC 9(3)  VALUE ZEROS.


       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 002-INPUT-STAR-NUM THRU 002-EXIT 
           PERFORM 001-PRINT-STAR-INLINE THRU 001-EXIT
              VARYING INDEX-NUM FROM 1 BY 1
              UNTIL INDEX-NUM > STAR-NUM 
           GOBACK 
           .
       
       001-PRINT-STAR-INLINE.
           MOVE ALL "*" TO SCR-LINE(1:INDEX-NUM)
           DISPLAY SCR-LINE 
           .

       001-EXIT.
           EXIT
           .

       002-INPUT-STAR-NUM.
           PERFORM UNTIL VALID-STAR-NUM 
                   DISPLAY "PLEASE INPUT STAR NUMBER: " WITH NO
                      ADVANCING 
                   ACCEPT STAR-NUM
                   IF NOT VALID-STAR-NUM DISPLAY "PLEASE INPUT STAR NUMBER IN PO
      -       "VE NUMBER "
           END-PERFORM
           
           .
           
       002-EXIT.
           EXIT
           .
